package br.com.suggestions.domain;
import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@SuppressWarnings("serial")
@MappedSuperclass
public class GenericDomain implements Serializable {
	
    @Id
    @GeneratedValue (strategy = GenerationType.TABLE)
    private Long id;

    public Long getCodigo() {
        return id;
    }

    public void setCodigo(Long id) {
        this.id = id;
    }
    
}