package br.com.suggestions.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Entity
public class Votacao extends GenericDomain {

    @ManyToOne
    @JoinColumn(name = "id_sugestao", nullable = false)
    private Sugestao sugestao;
    
    public Sugestao getSugestao() {
    	return sugestao;
    }
    
    public void setSugestao(Sugestao sugestao) {
    	this.sugestao = sugestao;
    }
	
    @ManyToOne
    @JoinColumn(name = "id_usuario", nullable = false)
    private Usuario usuario;
    
    public Usuario getUsuario() {
    	return usuario;
    }
    
    public void setUsuarioCriacao(Usuario usuario) {
    	this.usuario = usuario;
    }
    
    @Column(nullable = false)
    private String tipo;
    
    public String setTipo() {
        return tipo;
    }

    public void getTipo(String tipo) {
        this.tipo = tipo;
    }
	
}
