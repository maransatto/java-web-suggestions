package br.com.suggestions.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionEvent;

import org.omnifaces.util.Messages;

import br.com.suggestions.dao.SugestaoDAO;
import br.com.suggestions.dao.UsuarioDAO;
import br.com.suggestions.domain.Sugestao;
import br.com.suggestions.domain.Usuario;

@SuppressWarnings("serial")
@ManagedBean
@ViewScoped
public class SugestaoBean implements Serializable {
	
	private Sugestao sugestao = new Sugestao();
	private List<Sugestao> sugestoes;
	private List<Usuario> usuarios;

	public Sugestao getSugestao() {
		return sugestao;
	}

	public void setSugestao(Sugestao sugestao) {
		this.sugestao = sugestao;
	}

	public List<Sugestao> getSugestoes() {
		return sugestoes;
	}

	public void setSugestoes(List<Sugestao> sugestoes) {
		this.sugestoes = sugestoes;
	}
	
	public List<Usuario> getUsuarios() {
		return usuarios;
	}
	
	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}
	
	
	@SuppressWarnings("restriction")
	@PostConstruct
	public void listar() {
		try {
			SugestaoDAO sugestaoDAO = new SugestaoDAO();
			sugestoes = sugestaoDAO.listar();
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar listar os estados");
			erro.printStackTrace();
		}
	}
	public void novo() {
		
		try {
			sugestao = new Sugestao();
			
			UsuarioDAO usuarioDAO = new UsuarioDAO();
			usuarios = usuarioDAO.listar();
			
		} catch (RuntimeException erro) {
			Messages.addFlashGlobalError("Erro ao gerar uma nova sugest�o");
			erro.printStackTrace();
		}
	}

	public void salvar() {
		try {
			SugestaoDAO sugestaoDAO = new SugestaoDAO();
			sugestaoDAO.merge(sugestao);
			
			sugestao = new Sugestao();
			
			UsuarioDAO usuarioDAO = new UsuarioDAO();
			usuarios = usuarioDAO.listar();			
			
			sugestoes = sugestaoDAO.listar();
			
			Messages.addGlobalInfo("Sugest�o salva com sucesso");
			
		} catch (RuntimeException erro) {
			Messages.addGlobalError("Ocorreu um erro ao tentar salvar a sugestao");
			erro.printStackTrace();
		}
	}

	public void excluir(ActionEvent evento) {
		try {
			sugestao = (Sugestao) evento.getComponent().getAttributes().get("sugestaoSelecionado");

			SugestaoDAO sugestaoDAO = new SugestaoDAO();
			sugestaoDAO.excluir(sugestao);
			
			sugestoes = sugestaoDAO.listar();

			Messages.addGlobalInfo("Sugestao removida com sucesso");
		} catch (RuntimeException erro) {
			Messages.addFlashGlobalError("Ocorreu um erro ao tentar remover a sugestao");
			erro.printStackTrace();
		}
	}
	
	public void editar(ActionEvent evento){
		sugestao = (Sugestao) evento.getComponent().getAttributes().get("sugestaoSelecionado");
	}
}
