package br.com.suggestions.test;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import br.com.suggestions.dao.UsuarioDAO;
import br.com.suggestions.domain.Usuario;


public class UsuarioDAOTest {
	@Test
	public void salvar() {
		
		Usuario usuario= new Usuario();
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		
		usuario.setNome("Maranttop");
		
		usuarioDAO.salvar(usuario);
	}
	
	@Test
	@Ignore
	public void listar() {
		
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		List<Usuario> resultado = usuarioDAO.listar();
		
		System.out.println("Total de Registros encontrados: " + resultado.size());
		
		for (Usuario usuario : resultado) {
			System.out.println("Nome: " + usuario.getNome());
		}
		
	}
	
}
