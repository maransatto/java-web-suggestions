# Projeto de Java

Prof.: Rodrigo Ayres

Alunos: Fernando Silva Maransatto e Renan Zulian

## Modelagem
![Cria��o do Projeto](/img/modelagem.PNG)

## Etapas

### 1. Crie o projeto conforme as imagens abaixo

![Cria��o do Projeto](/img/create-project-1.PNG)

![Cria��o do Projeto](/img/create-project-2.PNG)

### 2. Atualize o pom.xml
### 3. Configure o arquivo de configura��o do Hibernate
### 4. Criar o pacote br.com.suggestions.domain
### 5. Criar as classes dentro do pacote domain
### 6. Incluir o mapeamento das classes dentro do hibernate.cfg.xml
### 7. Criar o HibernateUtil

Este � usado para executar o m�todo buildSessionFactory com base no nosso arquivo hibernate.cfg.xml para que crie no banco de dados toda a nossa estrutura.

### 8. Criar as classes Dao

Essas classes ser�o responsaveis pelos mentodos que executam a��es ou consultas diretas no banco de dados, atrav�s do hibernate.

### 9. Criar a classe Util do Hibernate para criar a estrutura no banco

Exemplo:
https://gitlab.com/maransatto/java-web-suggestions/commit/61777c0cc58a65ab605da9625db2e61c4b6525b0

### 10. Criar o WEB.INF

Exemplo:
https://gitlab.com/maransatto/java-web-suggestions/commit/678d7cc60fefef68c6bedc7f83529e9781a9fbeb

### 11. Criar arquivo XHTML

Exemplo:
https://gitlab.com/maransatto/java-web-suggestions/commit/1f689d016370368c92c5612c157f6503d85935fa

### 12. Cria arquivo Bean

Exemplo:
https://gitlab.com/maransatto/java-web-suggestions/commit/f62f843c075208be7bb9468de41598c76d54e1ef